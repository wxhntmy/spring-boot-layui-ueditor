package com.cwc.ueditor.mapper;

import com.cwc.ueditor.entity.ImgFile;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author wxhntmy
 */
@Repository
public interface ImgFileMapper {

    @Insert("insert into img_file(fileName,uuid,storeaddress,type) values(#{fileName},#{uuid},#{storeaddress},#{type})")
    int save(ImgFile fileImg);

    @Select("select * from img_file where uuid=#{uuid}")
    ImgFile findByUuid(String uuid);

    @Delete("delete from img_file where uuid=#{uuid}")
    int deleteByUuid(String uuid);

    @Select("select * from img_file where type = 'img' limit ${start},#{size}")
    List<ImgFile> getAllImgFiles(int start, int size);

    @Select("select count(*) from img_file where type = 'img'")
    int getAllImgFilesNum();

    @Select("select * from img_file limit ${start},#{size}")
    List<ImgFile> getAllFiles(int start, int size);

    @Select("select count(*) from img_file")
    int getAllFilesNum();
}
