package com.cwc.ueditor.mapper;

import com.cwc.ueditor.entity.Forum;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author wxhntmy
 */
@Repository
public interface ForumMapper {

    @Select("select * from forum where f_id=#{id}")
    Forum getForumById(String id);

    @Insert("insert into forum (f_id,title,stime,content) values (#{f_id},#{title},#{stime},#{content})")
    int addForum(Forum forum);

    @Select("select * from forum")
    List<Forum> getAllForums();
}
