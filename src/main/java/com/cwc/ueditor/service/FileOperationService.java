package com.cwc.ueditor.service;

import com.cwc.ueditor.entity.ImgFile;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * @author wxhntmy
 */
public interface FileOperationService {

    /**
     * 文件存放位置
     */
    String FILE_PATH = "E:\\IdeaProjects_Workspace\\ueditor\\src\\main\\resources\\upload";

    boolean saveFile(MultipartFile file, String uuid);

    Map<String, Object> getAllImgFiles(int start, int size);

    Map<String, Object> getAllFiles(int start, int size);

    File path(String filename);

    void download(String uuid, HttpServletRequest request, HttpServletResponse response);

    String getStr(HttpServletRequest request, String fileName);
}
