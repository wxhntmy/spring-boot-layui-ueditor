package com.cwc.ueditor.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class ImgFile implements Serializable {
    private String fileName,uuid,storeaddress,type;
}
