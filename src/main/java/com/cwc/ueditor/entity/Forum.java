package com.cwc.ueditor.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author wxhntmy
 */
@Setter
@Getter
public class Forum implements Serializable {
    private String f_id,title,stime,content;
}
