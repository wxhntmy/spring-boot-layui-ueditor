package com.cwc.ueditor.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author wxhntmy
 */
@Controller
public class PageController {

    @RequestMapping(value = "/ueditor", method = {RequestMethod.GET, RequestMethod.POST} )
    public String ueditor(){

        return "ueditor";
    }

    @RequestMapping(value = "/index", method = {RequestMethod.GET, RequestMethod.POST} )
    public String index(){

        return "index";
    }

    @RequestMapping(value = "/detail", method = {RequestMethod.GET, RequestMethod.POST} )
    public String detail(){

        return "detail";
    }

    @RequestMapping(value = "/forum", method = {RequestMethod.GET, RequestMethod.POST} )
    public String forum(@RequestParam String id, Model model){

        model.addAttribute("id", id);

        return "forum";
    }
}
