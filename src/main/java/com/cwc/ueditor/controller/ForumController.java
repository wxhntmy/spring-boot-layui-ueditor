package com.cwc.ueditor.controller;

import com.cwc.ueditor.entity.Forum;
import com.cwc.ueditor.mapper.ForumMapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author wxhntmy
 */
@RestController
public class ForumController {

    @Resource
    private ForumMapper forumMapper;

    @RequestMapping(value = "/forum/add_forum",method = {RequestMethod.POST, RequestMethod.GET})
    public Object add_forum(@RequestParam String content,
                          @RequestParam String title){

        Forum forum = new Forum();

        forum.setContent(content);
        forum.setTitle(title);
        forum.setF_id(UUID.randomUUID().toString());

        Date date = new Date();
        String strDateFormat = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(strDateFormat);
        forum.setStime(sdf.format(date));

        forumMapper.addForum(forum);

        Map<String, Object> map = new HashMap<>();
        map.put("message", "Success");
        map.put("ok",true);
        return map;
    }

    @RequestMapping(value = "/forum/get_all_forum",method = {RequestMethod.POST, RequestMethod.GET})
    public Object get_all_forum(){

        List<Forum> list = forumMapper.getAllForums();

        Map<String, Object> map = new HashMap<>();
        map.put("message", "Success");
        map.put("ok",true);
        map.put("data",list);
        return map;
    }

    @RequestMapping(value = "/forum/get_forum_by_id",method = {RequestMethod.POST, RequestMethod.GET})
    public Object get_forum_by_id(@RequestParam String id){

        Forum forum = forumMapper.getForumById(id);

        Map<String, Object> map = new HashMap<>();
        map.put("message", "Success");
        map.put("ok",true);
        map.put("data",forum);
        return map;
    }
}
