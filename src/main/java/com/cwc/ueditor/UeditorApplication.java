package com.cwc.ueditor;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author wxhntmy
 */
@SpringBootApplication
@MapperScan("com.cwc.ueditor.mapper")
public class UeditorApplication {

    public static void main(String[] args) {
        SpringApplication.run(UeditorApplication.class, args);
    }

}
