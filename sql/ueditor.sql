/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : localhost:3306
 Source Schema         : ueditor

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 07/05/2021 13:00:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for forum
-- ----------------------------
DROP TABLE IF EXISTS `forum`;
CREATE TABLE `forum`  (
  `f_id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `title` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `stime` datetime(0) NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`f_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forum
-- ----------------------------
INSERT INTO `forum` VALUES ('06f3e397-a8d5-41a6-9064-ac3dc5e6ebce', '测试文字', '2021-05-07 12:25:54', '<p>测试文字！！！</p><p><br/></p><p>测试文字！！！</p><p><br/></p><p><br/></p><p style=\"text-align: right;\">你好！！！！！！</p><p><br/></p><p><span style=\"font-size: 36px;\">啊啊啊啊啊啊啊啊啊</span></p><p><br/></p>');
INSERT INTO `forum` VALUES ('1119a6f7-c461-48fc-80b4-4243a7b644eb', '测试上传图片', '2021-05-07 12:53:43', '<p style=\"text-align: center;\"><span style=\"font-size: 36px;\">图片</span></p><p style=\"text-align:center\"><span style=\"font-size: 36px;\"><img src=\"/ueditor/download?uuid=b928eaed-5303-4d57-bd6f-84a309bcfce8\" title=\"地图流.jpg\" alt=\"地图流.jpg\" width=\"558\" height=\"343\"/></span></p><p><br/></p>');
INSERT INTO `forum` VALUES ('73a0c8dd-d359-4be6-8703-d6390fd6c568', '测试上传文件', '2021-05-07 12:54:27', '<p style=\"text-align: center;\"><span style=\"font-size: 36px;\">测试上传文件</span></p><p><span style=\"font-size: 36px;\"></span></p><p style=\"line-height: 16px;\"><img src=\"http://localhost:8090/ueditor/dialogs/attachment/fileTypeImages/icon_txt.gif\"/><a style=\"font-size:12px; color:#0066cc;\" href=\"/ueditor/download?uuid=a77b6fd2-6bea-474a-9da7-0acff778d1ca\" title=\"apache-tomcat-7.0.108.zip\">apache-tomcat-7.0.108.zip</a></p><p><span style=\"font-size: 36px;\"></span><br/></p>');
INSERT INTO `forum` VALUES ('ac6a96e4-9576-430e-aa3a-7e66343e60d6', '测试上传视频', '2021-05-07 12:55:28', '<p style=\"text-align: center;\"><span style=\"font-size: 36px;\">测试上传视频</span></p><p><span style=\"font-size: 36px;\"><br/></span></p><p style=\"text-align: center;\"><span style=\"font-size: 36px;\"><video class=\"edui-upload-video  vjs-default-skin video-js\" controls=\"\" preload=\"none\" width=\"420\" height=\"280\" src=\"/ueditor/download?uuid=e0b12b27-f145-499d-8c44-fae39fa4e39c\" data-setup=\"{}\"></video></span></p>');

-- ----------------------------
-- Table structure for img_file
-- ----------------------------
DROP TABLE IF EXISTS `img_file`;
CREATE TABLE `img_file`  (
  `uuid` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fileName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `storeaddress` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`uuid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of img_file
-- ----------------------------
INSERT INTO `img_file` VALUES ('412650ad-269d-4761-a587-1bd21854569b', '论文指正.png', 'E:\\IdeaProjects_Workspace\\ueditor\\src\\main\\resources\\upload');
INSERT INTO `img_file` VALUES ('52619509-24c8-488a-94ed-9ce5e0bb44cf', '1.mp4', 'E:\\IdeaProjects_Workspace\\ueditor\\src\\main\\resources\\upload');
INSERT INTO `img_file` VALUES ('5c1849c6-5d30-4193-bda4-cc62bb648325', 'apache-tomcat-7.0.108.zip', 'E:\\IdeaProjects_Workspace\\ueditor\\src\\main\\resources\\upload');
INSERT INTO `img_file` VALUES ('91a9da6c-445f-4eac-a0c2-e3c0d5a3e0fe', 'Thymeleaf3.png', 'E:\\IdeaProjects_Workspace\\ueditor\\src\\main\\resources\\upload');
INSERT INTO `img_file` VALUES ('a77b6fd2-6bea-474a-9da7-0acff778d1ca', 'apache-tomcat-7.0.108.zip', 'E:\\IdeaProjects_Workspace\\ueditor\\src\\main\\resources\\upload');
INSERT INTO `img_file` VALUES ('b928eaed-5303-4d57-bd6f-84a309bcfce8', '地图流.jpg', 'E:\\IdeaProjects_Workspace\\ueditor\\src\\main\\resources\\upload');
INSERT INTO `img_file` VALUES ('d1f212f8-6df3-4bbb-9129-33d8a10aacde', 'a.jpg', 'E:\\IdeaProjects_Workspace\\ueditor\\src\\main\\resources\\upload');
INSERT INTO `img_file` VALUES ('d7e54759-cf91-43f7-8274-1219ccd68fbb', '地图流.jpg', 'E:\\IdeaProjects_Workspace\\ueditor\\src\\main\\resources\\upload');
INSERT INTO `img_file` VALUES ('e0b12b27-f145-499d-8c44-fae39fa4e39c', '1.mp4', 'E:\\IdeaProjects_Workspace\\ueditor\\src\\main\\resources\\upload');
INSERT INTO `img_file` VALUES ('e1fbe0bc-a70e-44c1-b4bb-a1360e38ab45', '地图流.jpg', 'E:\\IdeaProjects_Workspace\\ueditor\\src\\main\\resources\\upload');
INSERT INTO `img_file` VALUES ('fa6693e3-2c6a-47c7-bd0d-8457ad9e7400', '论文指正.png', 'E:\\IdeaProjects_Workspace\\ueditor\\src\\main\\resources\\upload');

SET FOREIGN_KEY_CHECKS = 1;
